export interface Card {
    name: string;
    url: string;
  }
  
  export const cards = [
    {name: 'Angular', url: "https://angular.io/tutorial"},
    {name: 'Youtube', url: "https://www.youtube.com"},
    {name: 'Instagram', url: "https://www.instagram.com"}
  ];
  