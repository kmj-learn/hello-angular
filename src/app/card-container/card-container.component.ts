import { Component, Input } from '@angular/core';
import { cards } from 'src/data/cards';

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.css']
})
export class CardContainerComponent {
  cards = cards
}
