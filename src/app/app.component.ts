import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    title = 'hello'
    
    name = "Nur Hidayatullah 3J"

    message = false
    
    buttonLabel = "Open Message"

    ngOnInit(): void {
      console.warn("hello")
    }

    showMessage() {
        this.message = !this.message
        this.buttonLabel = true === this.message ? "Close Message":"Open Message"
    }
}
